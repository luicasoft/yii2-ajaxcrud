<?php

namespace lcaiza\ajaxcrud;

use yii\base\Widget;
use yii\helpers\Html;
use lcaiza\ajaxcrud\CrudAsset;
use lcaiza\ajaxcrud\BulkButtonWidget;

class AurySolutionsWidget extends Widget {

    public $message;

    public function init()
    {
        CrudAsset::register($this->getView());
        parent::init();
        if ($this->message === null)
        {
            $this->message = 'Sistema en Desarrollo por AurySolutions Ecuador';
        }
    }

    public function run()
    {
        return $this->render('index', [
            'message' => $this->message,
        ]);
    }

}