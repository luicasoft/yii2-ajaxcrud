<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
echo "<?php\n";
?>
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;
use lcaiza\ajaxcrud\AurySolutionsWidget;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;

?>

<?="<?="?>AurySolutionsWidget::widget()<?="?>\n"?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index container-fluid">
    <div id="ajaxCrudDatatable">
        <?="<?="?>GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    ((Helper::checkRoute('create')) ? Html::a('<i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Crear Nuevo','class'=>'btn btn-default']) : '').
                    Html::a('<i class="fa fa-redo"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Recargar Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="fa fa-list"></i> Listado de <?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?> ',
                'before'=>'<em>* Para cambiar el tamaño de las columnas arrastre los bordes de columna.</em>',
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'CSV'],
                GridView::PDF => [
                    'filename' => $this->title,
                ],
                GridView::EXCEL => [],
            ]
        ])<?="?>\n"?>
    </div>
</div>
<?='<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// Siempre lo necesitas para plugin jquery
    "options"=>[\'tabindex\' => false ],
    "size" => Modal::SIZE_LARGE
])?>'."\n"?>
<?='<?php Modal::end(); ?>'?>

